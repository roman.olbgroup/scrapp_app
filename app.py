from concurrent.futures import ThreadPoolExecutor
from itertools import repeat

from flask import Flask, request, render_template, redirect, url_for, flash
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc
from flask_login import UserMixin, login_user, LoginManager, login_required, logout_user, current_user

from settings import pages_range
from utils import parse_category_page, extend_ad_info

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
app.config['SECRET_KEY'] = "foobar"
db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

app.app_context().push()


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), nullable=False, unique=True)
    password = db.Column(db.String(), nullable=False)
    access_level = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<User %r>' % self.username


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(200), nullable=False)
    price = db.Column(db.Integer, nullable=False)
    picture = db.Column(db.String())
    author = db.Column(db.String(50))
    access_level = db.Column(db.Integer)

    def __repr__(self):
        return '<Post access_level=%r>' % self.access_level


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@app.route("/login", methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        user = User.query.filter_by(username=username).first()
        if user:
            if password == user.password:
                login_user(user)
                return redirect(url_for('home'))
            else:
                flash("Incorrect password. Please try again.")
        else:
            flash('User doesn\'t exists')
            return redirect(url_for('login'))
    return render_template('login.html')


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route("/")
@login_required
def home():
    return render_template('home.html')


@app.route("/posts")
@login_required
def get_posts():
    # start = time.monotonic()
    access_level = current_user.access_level
    result = []
    with ThreadPoolExecutor(max_workers=10) as executor:
        for items in executor.map(parse_category_page, range(1, pages_range[access_level-1] + 1)):
            result += items
    if access_level == 2 or access_level == 3:
        with ThreadPoolExecutor(max_workers=300) as executor:
            executor.map(extend_ad_info, result, repeat(access_level))

    if access_level == 1:
        result = result[:100]
    elif access_level == 2:
        result = result[:200]
    else:
        result = result[:300]

    rows = [
        Post(
            title=post.get('title'),
            price=post.get('price'),
            picture=post.get('picture'),
            author=post.get('author'),
            access_level=access_level
        ) for post in result
    ]
    try:
        db.session.add_all(rows)
        db.session.commit()
    except exc.SQLAlchemyError:
        pass  # some error handling might be here

    for i in range(len(result)):
        post = result[i]
        del post['ad_link']
        post['access_level'] = access_level
        post['id'] = rows[i].id
    # finish = time.monotonic()
    # finished_after = finish - start
    # print('finished_after:', finished_after)

    return result


@app.route("/post/delete")
@login_required
def delete_post():
    post_id = request.args.get('id')
    post = db.session.get(Post, post_id)
    if post:
        try:
            db.session.delete(post)
            db.session.commit()

        except exc.SQLAlchemyError:
            pass  # some error handling might be here
    flash(f'Post with id {post_id} was successfully deleted from DB!')
    return {'success': True}


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
