const button = document.querySelector(".refresh-button");
const wrapper = document.querySelector(".main-wrapper");

let items = [];
let reverseSort = true;

const sortRows = () => {
   items.reverse();
   reverseSort = !reverseSort;
};

const handleSort = () => {
   sortRows();
   displayRows();
};

function displayRows() {
   const prevWrapper = document.querySelector(".rows-wrapper");
   if (prevWrapper) {
      prevWrapper.remove();
   }

   const rowsWrapper = document.createElement("div");
   rowsWrapper.setAttribute("class", "rows-wrapper");
   rowsWrapper.innerHTML = items.map((item) => generateRow(item)).join("");

   const sortButton = document.createElement("button");
   sortButton.innerHTML = reverseSort ? "ASC ↑" : "DESC ↓";
   sortButton.setAttribute("onclick", "handleSort()");
   sortButton.setAttribute("class", "sort-button");

   rowsWrapper.prepend(sortButton);
   wrapper.appendChild(rowsWrapper);
}

const deleteRow = async (row_id) => {
   const res = await fetch(`http://0.0.0.0:5000/post/delete?id=${row_id}`);
   if (res.status === 200) {
      items = items.filter(({ id }) => id !== row_id);
      displayRows();
   }
};

const generateRow = (item) => {
   const { id, title, price, author, picture } = item;
   const image = picture
      ? `<div class="image"><img src=${picture} alt="" /></div>`
      : "";
   const adAuthor = author ? `<p>${author}</p>` : "";
   const row = `
   <div class="row">
    ${image}
    <div class="row-info">
      <p>${title}</p>
      <p>${price} UAH</p>
       ${adAuthor}
    </div>
    <button class='row-button' onclick='deleteRow(${id})'>Delete</button>
  </div>
   `;
   return row;
};

const fetchItems = async () => {
   button.innerHTML = "Loading posts...";
   const res = await fetch("http://0.0.0.0:5000/posts");
   items = await res.json();
   items.sort((a, b) => a.price - b.price);
   button.remove();
   displayRows();
   reverseSort = true;
};

button.addEventListener("click", fetchItems);
