# import time
import re

import requests
from bs4 import BeautifulSoup

from settings import BASE_URL, default_category_path


def parse_category_page(page_number: int):
    url = BASE_URL + default_category_path.format(page_number)
    res = requests.get(url=url)
    soup = BeautifulSoup(res.text, 'html.parser')
    ads = soup.find_all("div", attrs={"data-cy": "l-card"})
    result = []
    if ads:
        for ad in ads:
            price = ad.find('p', attrs={"data-testid": "ad-price"}).contents[0]
            price = int(''.join(re.findall(r'\d+', price)))
            item = {
                'ad_link': ad.find('a').get('href'),
                'title': ad.find('h6').contents[0],
                'price': price,
            }
            result.append(item)
    else:
        ads = soup.find_all("div", attrs={"class": "offer-wrapper"})
        for ad in ads:
            price = ad.find('p', attrs={"class": "price"}).find('strong').contents[0]
            price = int(''.join(re.findall(r'\d+', price)))
            item = {
                'ad_link': ad.find('a').get('href'),
                'title': ad.find('a', attrs={'data-cy': 'listing-ad-title'}).find('strong').contents[0],
                'price': price,
            }
            result.append(item)
    return result


def extend_ad_info(ad: dict[str, any], access_level: int):
    # start = time.monotonic()
    url = ad['ad_link']
    if BASE_URL not in url:
        url = BASE_URL + url
    res = requests.get(url=url)
    soup = BeautifulSoup(res.text, 'html.parser')
    picture = soup.find('div', attrs={"data-cy": "adPhotos-swiperSlide"}).find('img').get('src')
    add_info = {
        'picture': picture
    }
    if access_level == 3:
        author = soup.find('h4').contents[0]
        add_info.update({'author': author})
    # finish = time.monotonic()
    # finished = finish - start
    # print(f'finished at: {round(finished, 3)}')
    ad.update(add_info)